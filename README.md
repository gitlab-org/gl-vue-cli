# gl-vue-cli
`gl-vue-cli` is a CLI that scalffold a Vue and Vuex app based on gitlab's documentation

## Usage
`gl-vue-cli` has to be run from `<path-to-gdk>/gitlab/`

```bash
  cd <path-to-gdk>/gitlab
  gl-vue-cli
```

### Options
// todo

## Contributing

Please refer to [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## License
[LICENSE](LICENSE)
