#!/usr/bin/env node
'use strict'

const capitalize = require('lodash.capitalize')

module.exports = (namespace) => {
  const capitalizedNamesapce = capitalize(namespace)

  const template = `<script>
import { mapGetters, mapState, mapActions } from 'vuex';
import { GlLoadingIcon } from '@gitlab-org/gitlab-ui';

export default {
  name: '${namespace}App',
  components: {
    GlLoadingIcon,
  },
  props: {
    endpoint: {
      type: String,
      required: true,
    }
  },
  computed: {
    ...mapState([
      'isLoading',
      'data',
      'hasError',
    ]),
    ...mapGetters([]),
  },
  created() {
    this.setEndpoint(this.endpoint);
    this.fetch${capitalizedNamesapce}();
  },
  methods: {
    ...mapActions([
      'setEndpoint',
      'fetch${capitalizedNamesapce}'
    ]),
  }
};
</script>
<template>
  <div>
    <gl-loading-icon
      v-if="isLoading"
      :size="2"
      class="js-loading qa-loading-animation prepend-top-20"
    />
    <div 
      v-else-if="hasError"
      class="js-error-state"
    >
      {{ __('Something went wrong. Please try again') }}
    </div>
    <div 
      v-else
      class="js-success-state"
    >
      {{ data }}
    </div>
  </div>
</template>
  `

  return template
}
