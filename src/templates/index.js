#!/usr/bin/env node
'use strict'

const actionsTemplate = require('./store/actions_template')
const actionsEtagTemplate = require('./store/etag_fetch_actions_template')
const storeIndexTemplate = require('./store/index_template')
const mutationsTemplate = require('./store/mutations_template')
const mutationTypesTemplate = require('./store/mutation_types_template')
const stateTemplate = require('./store/state_template')
const gettersTemplate = require('./store/getters_template')
const indexTemplate = require('./index_template')
const specActionsTemplate = require('./specs/actions_spec_template')
const specActionsEtagTemplate = require('./specs/etag_actions_spec_template')
const specMutationsTemplate = require('./specs/mutations_spec_template')
const specAppTemplate = require('./specs/app_spec_template')
const specGettersTemplate = require('./specs/getters_spec')
const appTemplate = require('./components/app_template')

module.exports = {
  actionsTemplate,
  actionsEtagTemplate,
  storeIndexTemplate,
  mutationsTemplate,
  mutationTypesTemplate,
  stateTemplate,
  gettersTemplate,
  indexTemplate,
  specActionsTemplate,
  specActionsEtagTemplate,
  specMutationsTemplate,
  specGettersTemplate,
  specAppTemplate,
  appTemplate,
}
