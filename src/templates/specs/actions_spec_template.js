#!/usr/bin/env node
'use strict'

const capitalize = require('lodash.capitalize')

module.exports = (namespace, path) => {
  const capitalizedNamesapce = capitalize(namespace)
  const upperCasedNamespace = namespace.toUpperCase()

  return `import MockAdapter from 'axios-mock-adapter';
import axios from '~/lib/utils/axios_utils';
import {
  setEndpoint,
  request${capitalizedNamesapce},
  fetch${capitalizedNamesapce},
  receive${capitalizedNamesapce}Success,
  receive${capitalizedNamesapce}Error,
} from '~/${path}/actions';
import state from '~/${path}/state';
import * as types from '~/${path}/mutation_types';
import testAction from 'spec/helpers/vuex_action_helper';

describe('${capitalizedNamesapce} State actions', () => {
  let mockedState;

  beforeEach(() => {
    mockedState = state();
  });

  describe('setEndpoint', () => {
    it('should commit SET_ENDPOINT mutation', done => {
      testAction(
        setEndpoint,
        'endpoint.json',
        mockedState,
        [{ type: types.SET_ENDPOINT, payload: 'endpoint.json' }],
        [],
        done,
      );
    });
  });

  describe('request${capitalizedNamesapce}', () => {
    it('should commit REQUEST_${upperCasedNamespace} mutation', done => {
      testAction(request${capitalizedNamesapce}, null, mockedState, [{ type: types.REQUEST_${upperCasedNamespace} }], [], done);
    });
  });

  describe('fetch${capitalizedNamesapce}', () => {
    let mock;

    beforeEach(() => {
      mockedState.endpoint = 'endpoint.json;'
      mock = new MockAdapter(axios);
    });

    afterEach(() => {
      mock.restore();
    });

    describe('success', () => {
      it('dispatches request${capitalizedNamesapce} and receive${capitalizedNamesapce}Success ', done => {
        mock.onGet('endpoint.json').replyOnce(200, { id: 121212 });

        testAction(
          fetch${capitalizedNamesapce},
          null,
          mockedState,
          [],
          [
            {
              type: 'request${capitalizedNamesapce}',
            },
            {
              payload: { id: 121212 },
              type: 'receive${capitalizedNamesapce}Success',
            },
          ],
          done,
        );
      });
    });

    describe('error', () => {
      beforeEach(() => {
        mock.onGet('endpoint.json').reply(500);
      });

      it('dispatches request${capitalizedNamesapce} and receive${capitalizedNamesapce}Error ', done => {
        testAction(
          fetch${capitalizedNamesapce},
          null,
          mockedState,
          [],
          [
            {
              type: 'request${capitalizedNamesapce}',
            },
            {
              type: 'receive${capitalizedNamesapce}Error',
            },
          ],
          done,
        );
      });
    });
  });

  describe('receive${capitalizedNamesapce}Success', () => {
    it('should commit RECEIVE_${upperCasedNamespace}_SUCCESS mutation', done => {
      testAction(
        receive${capitalizedNamesapce}Success,
        { id: 121232132 },
        mockedState,
        [{ type: types.RECEIVE_${upperCasedNamespace}_SUCCESS, payload: { id: 121232132 } }],
        [],
        done,
      );
    });
  });

  describe('receive${capitalizedNamesapce}Error', () => {
    it('should commit RECEIVE_${upperCasedNamespace}_ERROR mutation', done => {
      testAction(receive${capitalizedNamesapce}Error, null, mockedState, [{ type: types.RECEIVE_${upperCasedNamespace}_ERROR }], [], done);
    });
  });
});
  `
}
