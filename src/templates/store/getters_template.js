#!/usr/bin/env node
'use strict'

module.exports = () => {
  return `/**
* This file has examples on how to create a getter.
* You should delete them and follow the same structure when creating your owns
*/

/**
 * This is an example.
 * Make sure you add jsdoc to your getters.
 * 
 * @param {Object} state 
 * @returns {String} the value of foo
 */
export const getter1 = state => state.foo;

/**
 * This is an example.
 * Make sure you add jsdoc to your getters.
 * 
 * @param {Object} state 
 * @returns {Boolean}
 */
export const getter2 = state => state.isLoading;  

// prevent babel-plugin-rewire from generating an invalid default during karma tests
export default () => {};
`
}