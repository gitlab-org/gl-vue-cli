#!/usr/bin/env node
'use strict'

module.exports = (namespace) => {
  const upperCaseNamespace = namespace.toUpperCase()

  return `/** 
  * Instructions:
  * These are the regular mutations for an app that fetches data.
  * Replace the content in _NAMESPACE_ with a more suitable name, for example, REQUEST_JOBS
  * 
  * For actions that make requests to the API we follow this pattern: https://docs.gitlab.com/ce/development/fe_guide/vuex.html#actions-pattern-request-and-receive-namespaces
  * For every request we have 3 mutations:
  * 1. REQUEST_NAMESPACE: It's usually used to toggle the loading prop
  * 2. RECEIVE_NAMESPACE_SUCCESS: It's used to store the data of a successfull request
  * 3. RECEIVE_NAMESAPCE_ERROR: It's used to handle the error 
  * This pattern allows that all applications follow the same flow and that our actions have a human friendly format.
 */
 
export const SET_ENDPOINT = 'SET_ENDPOINT';

export const REQUEST_${upperCaseNamespace} = 'REQUEST_${upperCaseNamespace}';
export const RECEIVE_${upperCaseNamespace}_SUCCESS = 'RECEIVE_${upperCaseNamespace}_SUCCESS';
export const RECEIVE_${upperCaseNamespace}_ERROR = 'RECEIVE_${upperCaseNamespace}_ERROR';
  `
}
