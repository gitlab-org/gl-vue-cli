#!/usr/bin/env node
'use strict'

const path = require('path')
const snakeCase = require('lodash.snakeCase')
const fs = require('fs-extra')
const {
  actionsTemplate,
  actionsEtagTemplate,
  storeIndexTemplate,
  mutationsTemplate,
  mutationTypesTemplate,
  indexTemplate,
  appTemplate,
  stateTemplate,
  gettersTemplate
} = require('./templates/index')

/**
 * scaffold the files inside the `assets` folder:
 *  - namespace
 *    - store
 *      | - index.js
 *      | - state.js
 *      | - actions.js
 *      | - mutations.js
 *      | - mutations_types.js
 *      | - getters.js
 *    - components
 *      |- app.js
 *    - index.js
 * 
 * 
 * @param {Object} answers
 */

module.exports = (answers) => {
  const folderNamespace = snakeCase(answers.namespace)
  const namespace = snakeCase(answers.namespace)

  const domID = answers.domID
  const polling = answers.polling

  const assetsPath = path.join('app', 'assets', 'javascripts', folderNamespace)
  const actionsPath = path.join(assetsPath, 'store', 'actions.js')

  // main index.js file
  fs.outputFile(path.join(assetsPath, 'index.js'), indexTemplate(domID))

  // store files
  fs.outputFile(path.join(assetsPath, 'store', 'index.js'), storeIndexTemplate(domID))
  fs.outputFile(path.join(assetsPath, 'store', 'state.js'), stateTemplate())
  fs.outputFile(path.join(assetsPath, 'store', 'getters.js'), gettersTemplate())
  fs.outputFile(path.join(assetsPath, 'store', 'mutations.js'), mutationsTemplate(namespace))
  fs.outputFile(path.join(assetsPath, 'store', 'mutation_types.js'), mutationTypesTemplate(namespace))

  if (polling === 'etag') {
    fs.outputFile(actionsPath, actionsEtagTemplate(namespace))
  } else {
    fs.outputFile(actionsPath, actionsTemplate(namespace))
  }

  // app file
  fs.outputFile(path.join(assetsPath, 'components', 'app.vue'), appTemplate(namespace))
}
