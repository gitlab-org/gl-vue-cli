#!/usr/bin/env node
'use strict'

const { hasAssestsNamespace, hasSpecsNamesapce, error } = require('./utils')

/**
 * List of questions that will be prompted when starting the cli
 */
module.exports = [
  {
    type: 'input',
    name: 'namespace',
    message:
      'Welcome to gl-vue-cli. What is the namespace of your new feature?',
    validate(input) {
      if (!input.length) {
        return error('Please specify the namespace of your new feature')
      }

      if (hasAssestsNamespace(input)) {
        return error('A folder with this name already exists')
      }

      if (hasSpecsNamesapce(input)) {
        return error('A spec folder with this name already exists')
      }
     
      return true
    }
  },
  {
    type: 'input',
    name: 'domID',
    message: 'What is the ID of the DOM element where the app will be mounted?',
    validate(input) {
      return !input.length ? error('Please specify the ID of your new feature') : true
    }
  },
  {
    type: 'list',
    name: 'polling',
    message:
      'Does the main fetch request of your application use etag polling?',
    choices: [
      {
        name: 'eTag Polling',
        value: 'etag',
        short: 'etag'
      },
      {
        name: 'won\'t poll',
        value: 'no',
        short: 'no polling'
      }
    ]
  }
]
