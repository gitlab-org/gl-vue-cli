#!/usr/bin/env node
'use strict'
const inquirer = require('inquirer')
const questions = require('./questions')
const assetsScaffold = require('./scaffold_assets')
const specsScaffold = require('./scaffold_specs')
const { createIssueError } = require('./utils')

inquirer
  .prompt(questions)
  .then(answers => {
    assetsScaffold(answers)
    specsScaffold(answers)
  })
  .catch(err => createIssueError(err))
